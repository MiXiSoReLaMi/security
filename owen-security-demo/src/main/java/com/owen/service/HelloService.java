package com.owen.service;

public interface HelloService {

    String greeting(String name);
}
