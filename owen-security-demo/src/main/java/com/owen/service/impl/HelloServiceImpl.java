package com.owen.service.impl;

import com.owen.service.HelloService;
import org.springframework.stereotype.Service;

@Service
public class HelloServiceImpl implements HelloService {

    @Override
    public String greeting(String name) {
        System.out.println("初始化service");
        return "hello " + name;
    }
}
